<?php

namespace App\Controller;

use App\Services\Widget\WidgetService;
use App\Services\Widget\ClientService;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/widget", name="widget")
 * @throws ORMException
 */
class WidgetController extends AbstractController
{
    private const PROLONGATION = 'prolongation';
    private const USE = 'use';

    private $session;

    /**
     * @var widgetService
     */
    private $widgetService;

    public function __construct(SessionInterface $session, EntityManagerInterface $em, WidgetService $widgetService)
    {
        $this->session = $session;
        $this->widgetService = $widgetService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(Request $request)
    {
        $widgetAction = null;
        if($request->request->has('act')){
            $widgetAction = $request->request->get('act');
        }

        if(empty($this->session->get('user_id'))){
            return new JsonResponse(
                'Session user_id is empty',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        if(empty($this->session->get('token'))){
            return new JsonResponse(
                'Session token is empty',
                Response::HTTP_INTERNAL_SERVER_ERROR
            );
        }

        $widgetDeals = new ClientService();
        $widgetDeals->setUserId($this->session->get('user_id'));
        $widgetDeals->setToken($this->session->get('token'));
        $widgetDeals->setUserIp($_SERVER['REMOTE_ADDR']);

        if ($widgetAction === self::PROLONGATION) {
            return $this->widgetService
                ->prolongation($widgetDeals, $request->get('code'));
        }

        if ($widgetAction === self::USE) {
            return $this->widgetService
                ->use($widgetDeals, $request->get('code'));
        }

        return $this->widgetService
            ->get($widgetDeals, $request->get('code'));
    }
}
