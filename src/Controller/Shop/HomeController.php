<?php

namespace App\Controller\Shop;

use App\Component\Discount\Model\Discount;
use App\Component\Order\OrderFactory;
use App\Component\Product\Repository\ProductRepository;
use App\Services\Widget\ClientService;
use App\Services\Widget\DisplayService;
use App\Services\Widget\WidgetService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\MakerBundle\Str;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var DisplayService
     */
    private $widgetDisplayService;

    /**
     * @var WidgetService
     */
    private $widgetService;

    /**
     * BasketController constructor.
     * @param DisplayService $widgetDisplayService
     * @param WidgetService $widgetService
     */
    public function __construct(DisplayService $widgetDisplayService, WidgetService $widgetService, SessionInterface $session){
        $this->widgetDisplayService = $widgetDisplayService;
        $this->widgetService = $widgetService;
        $this->session = $session;
    }


    /**
     * @Route("/", name="home")
     */
    public function index(ProductRepository $productRepository, OrderFactory $order, Request $request): Response
    {
        #todo отсюда вызывается виджет
        if(is_object($this->session->get('token'))){
            $this->session->set('token', $this->session->get('token')->token);
        }

        $widgetDealsHtml = $this->widgetDisplayService->getWidgetDisplay(
            $this->session->get('user_id'),
            $this->session->get('token'),
            2
        );


        $products = $productRepository->findAll();
        #todo пример вызова шаблона и продуктов магазина
        return $this->render('home/index.html.twig', [
            'itemsInCart' => $order->getCurrent()->getItemsTotal(),
            'products' => $products,
            'widgetDealsHtml' => $widgetDealsHtml // Переменная вывода виджета
        ]);
    }
}
